using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace NHibernateDemo1.Tests
{
    [TestFixture]
    public class CreateSchemaTests
    {
        private Configuration _configuration;

        [SetUp]
        public void SetUp()
        {
            _configuration = new Configuration();
            _configuration.Configure();
        }

        [Test]
        public void CanGenerateSchema()
        {
            var export = new SchemaExport(_configuration);
            export.Execute(false, true, false, false);
        }
    }
}