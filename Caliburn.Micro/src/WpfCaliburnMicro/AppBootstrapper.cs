using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Caliburn.Micro;
using Ninject;
using WpfCaliburnMicro.DependencyResolution;
using WpfCaliburnMicro.Infrastructure;
using WpfCaliburnMicro.ViewModels;

namespace WpfCaliburnMicro
{
    public class AppBootstrapper : Bootstrapper<MainViewModel>
    {
        private IKernel _kernel;
        private static ILog _log;

        public AppBootstrapper()
        {
            LogManager.GetLog = type => new DebugLogger(type);
            _log = LogManager.GetLog(typeof (AppBootstrapper));

            AddCustomConventions();

            _log.Info("Application started...");
        }

        protected override void Configure()
        {
            _kernel = new StandardKernel(new CaliburnModule(), new ViewModelModule());
            _kernel.Bind<IKernel>().ToConstant(_kernel);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            var instance = _kernel.Get(serviceType ?? typeof (object), key);

            if (instance == null)
                throw new Exception(String.Format("Could not locate any instances of contract {0}.", key));

            return instance;
        }

        private static void AddCustomConventions()
        {
            ConventionManager.AddElementConvention<DatePicker>(DatePicker.TextProperty, "Text", "TextChanged");

            //ConventionManager.AddElementConvention<AutoCompleteBox>(
            //    AutoCompleteBox.ItemsSourceProperty, "DataContext", "Loaded");
            //ConventionManager.AddElementConvention<DataGrid>(
            //    DataGrid.ItemsSourceProperty, "DataContext", "Loaded");
            //ConventionManager.AddElementConvention<HyperlinkButton>(
            //    HyperlinkButton.NavigateUriProperty, "DataContext", "Click");
            //ConventionManager.AddElementConvention<NumericUpDown>(
            //    NumericUpDown.ValueProperty, "Value", "ValueChanged");
        }
    }
}