using System;
using System.Collections.Generic;
using Caliburn.Micro;
using WpfCaliburnMicro.Messages;
using WpfCaliburnMicro.Results;

namespace WpfCaliburnMicro.ViewModels
{
    public class ContactsViewModel : Screen
    {
        private readonly IEventAggregator _eventAggregator;
        private string _firstName;
        private string _lastName;
        private string _phoneNumber;
        private string _mobileNumber;
        private DateTime? _birthDate;

        public ContactsViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                NotifyOfPropertyChange(() => FirstName);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                _phoneNumber = value;
                NotifyOfPropertyChange(() => PhoneNumber);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                _mobileNumber = value;
                NotifyOfPropertyChange(() => MobileNumber);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public DateTime? BirthDate
        {
            get { return _birthDate; }
            set
            {
                _birthDate = value;
                NotifyOfPropertyChange(() => BirthDate);
                NotifyOfPropertyChange(() => Age);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public int Age
        {
            get
            {
                if (BirthDate == null)
                    return 0;
                return DateTime.Today.Year - BirthDate.Value.Year;
            }
        }

        public bool CanSave
        {
            get
            {
                return
                    !String.IsNullOrWhiteSpace(FirstName) &&
                    !String.IsNullOrWhiteSpace(LastName) &&
                    !String.IsNullOrWhiteSpace(PhoneNumber) &&
                    !String.IsNullOrWhiteSpace(MobileNumber) &&
                    Age > 15 && Age < 100;
            }
        }

        public void Clear()
        {
            FirstName = String.Empty;
            LastName = String.Empty;
            PhoneNumber = String.Empty;
            MobileNumber = String.Empty;
            BirthDate = null;
        }

        public IEnumerable<IResult> Save()
        {
            var saveResult = new SaveContactResult(FirstName, LastName);
            yield return saveResult;

            if (!saveResult.IsSuccessful)
            {
                yield return MessageBoxResult.Show("Error saving contact");
                yield break;
            }

            var contactName = FirstName + " " + LastName;
            var success = new SuccessViewModel {ContactName = contactName};
            yield return ShowDialog.Instance(success);

            Clear();

            _eventAggregator.Publish(
                new MainMessageChangeRequest {Message = "The contact " + contactName + " is saved..."});
        }
    }
}