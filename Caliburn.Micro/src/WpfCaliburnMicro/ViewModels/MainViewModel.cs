using Caliburn.Micro;
using WpfCaliburnMicro.Messages;

namespace WpfCaliburnMicro.ViewModels
{
    public class MainViewModel : Screen, IHandle<MainMessageChangeRequest>
    {
        public MainViewModel(CalculatorViewModel calculator, ContactsViewModel contacts, IEventAggregator eventAggregator)
        {
            Calculator = calculator;
            Contacts = contacts;

            eventAggregator.Subscribe(this);
        }

        public CalculatorViewModel Calculator { get; private set; }
        public ContactsViewModel Contacts { get; private set; }

        public string Message { get; set; }

        public void Handle(MainMessageChangeRequest request)
        {
            Message = request.Message;
            NotifyOfPropertyChange(() => Message);
        }
    }
}