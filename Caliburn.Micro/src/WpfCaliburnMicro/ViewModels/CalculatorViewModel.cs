using Caliburn.Micro;

namespace WpfCaliburnMicro.ViewModels
{
    public class CalculatorViewModel : Screen
    {
        private double _x;
        private double _y;
        private double _result;

        public double Result
        {
            get { return _result; }
            set
            {
                _result = value;
                NotifyOfPropertyChange(() => Result);
            }
        }

        public double X
        {
            get { return _x; }
            set
            {
                _x = value;
                NotifyOfPropertyChange(() => X);
                NotifyOfPropertyChange(() => CanAdd);
            }
        }

        public double Y
        {
            get { return _y; }
            set
            {
                _y = value;
                NotifyOfPropertyChange(() => Y);
                NotifyOfPropertyChange(() => CanAdd);
            }
        }

        public bool CanAdd
        {
            get { return X > 0 && Y > 0; } 
        }
        
        public void Add()
        {
            Result = X + Y;
        }
    }
}