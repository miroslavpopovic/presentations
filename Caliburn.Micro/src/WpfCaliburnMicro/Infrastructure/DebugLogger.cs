using System;
using System.Diagnostics;
using Caliburn.Micro;

namespace WpfCaliburnMicro.Infrastructure
{
    public class DebugLogger : ILog
    {
        private readonly string _typeName;

        public DebugLogger(Type type)
        {
            _typeName = type.FullName;
        }

        public void Info(string format, params object[] args)
        {
            Debug.WriteLine("* INFO * {0}", (object) _typeName);
            Debug.WriteLine(format, args);
        }

        public void Warn(string format, params object[] args)
        {
            Debug.WriteLine("* WARN * {0}", (object) _typeName);
            Debug.WriteLine(format, args);
        }

        public void Error(Exception exception)
        {
            Debug.WriteLine("* ERROR * {0}", (object) _typeName);
            Debug.WriteLine(exception.ToString());
        }
    }
}