using System;
using Caliburn.Micro;

namespace WpfCaliburnMicro.Results
{
    public class SaveContactResult : IResult
    {
        private readonly string _firstName;
        private readonly string _lastName;

        public SaveContactResult(string firstName, string lastName)
        {
            _firstName = firstName;
            _lastName = lastName;
        }

        public bool IsSuccessful { get; set; }

        public void Execute(ActionExecutionContext context)
        {
            // Start some asynchronous long-running operation...
            // ContactSaveService.SaveAsync();...

            // Call asynchronous callback
            OnSaveCompleted();
        }

        private void OnSaveCompleted()
        {
            //if (ThereAreErrors)
            //{
            //    Completed(this, new ResultCompletionEventArgs{Error = Exception});
            //    return;
            //}

            IsSuccessful = true;
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };
    }
}