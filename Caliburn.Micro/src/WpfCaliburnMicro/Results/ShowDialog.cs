using System;
using Caliburn.Micro;

namespace WpfCaliburnMicro.Results
{
    public class ShowDialog : IResult
    {
        private readonly Type _dialogType;
        private readonly object _instance;

        public ShowDialog(Type dialogType)
        {
            _dialogType = dialogType;
        }

        public ShowDialog(object instance)
        {
            _instance = instance;
        }

        public void Execute(ActionExecutionContext context)
        {
            var dialog = _instance ?? IoC.GetInstance(_dialogType, null);
            var windowManager = IoC.Get<IWindowManager>();

            windowManager.ShowDialog(dialog);
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static ShowDialog Of<T>()
        {
            return new ShowDialog(typeof (T));
        }

        public static ShowDialog Instance(object instance)
        {
            return new ShowDialog(instance);
        }
    }
}