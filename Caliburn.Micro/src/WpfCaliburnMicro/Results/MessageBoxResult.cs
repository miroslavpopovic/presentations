using System;
using System.Windows;
using Caliburn.Micro;

namespace WpfCaliburnMicro.Results
{
    public class MessageBoxResult : IResult
    {
        private readonly string _message;
        private readonly string _caption;
        private readonly MessageBoxButton _button;

        public MessageBoxResult(string message, string caption, MessageBoxButton button)
        {
            _message = message;
            _caption = caption;
            _button = button;
        }

        public void Execute(ActionExecutionContext context)
        {
            MessageBox.Show(_message, _caption, _button);
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static MessageBoxResult Show(string message)
        {
            return Show(message, "Caliburn.Micro demo", MessageBoxButton.OK);
        }

        public static MessageBoxResult Show(string message, string caption)
        {
            return Show(message, caption, MessageBoxButton.OK);
        }

        public static MessageBoxResult Show(string message, string caption, MessageBoxButton button)
        {
            return new MessageBoxResult(message, caption, button);
        }
    }
}