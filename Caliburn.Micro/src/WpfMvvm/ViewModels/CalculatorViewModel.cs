using System.ComponentModel;
using System.Windows.Input;
using WpfMvvm.Infrastructure;

namespace WpfMvvm.ViewModels
{
    public class CalculatorViewModel : INotifyPropertyChanged
    {
        private double _x;
        private double _y;
        private double _result;

        public CalculatorViewModel()
        {
            AddCommand = new DelegateCommand(OnAdd, CanAdd);
        }

        public ICommand AddCommand { get; private set; }

        public double Result
        {
            get { return _result; }
            set 
            { 
                _result = value;
                OnPropertyChanged("Result");
            }
        }

        public double X 
        {
            get { return _x; }
            set 
            { 
                _x = value;
                OnPropertyChanged("X");
            }
        }

        public double Y 
        {
            get { return _y; }
            set 
            { 
                _y = value;
                OnPropertyChanged("Y");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate {};

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool CanAdd()
        {
            return X > 0 && Y > 0;
        }

        private void OnAdd()
        {
            Result = X + Y;
        }
    }
}