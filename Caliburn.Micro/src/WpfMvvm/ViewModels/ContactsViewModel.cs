using System;
using System.ComponentModel;
using System.Windows.Input;
using WpfMvvm.Infrastructure;
using WpfMvvm.Views;

namespace WpfMvvm.ViewModels
{
    public class ContactsViewModel : INotifyPropertyChanged
    {
        private string _firstName;
        private string _lastName;
        private string _phoneNumber;
        private string _mobileNumber;
        private DateTime? _birthDate;

        public ContactsViewModel()
        {
            ClearCommand = new DelegateCommand(OnClear);
            SaveCommand = new DelegateCommand(OnSave, CanSave);
        }

        public ICommand ClearCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }

        public string FirstName
        {
            get { return _firstName; }
            set 
            { 
                _firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set 
            { 
                _lastName = value;
                OnPropertyChanged("LastName");
            }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set 
            { 
                _phoneNumber = value;
                OnPropertyChanged("PhoneNumber");
            }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set 
            { 
                _mobileNumber = value;
                OnPropertyChanged("MobileNumber");
            }
        }

        public DateTime? BirthDate
        {
            get { return _birthDate; }
            set 
            { 
                _birthDate = value;
                OnPropertyChanged("BirthDate");
                OnPropertyChanged("Age");
            }
        }

        public int Age
        {
            get
            {
                if (BirthDate == null)
                    return 0;
                return DateTime.Today.Year - BirthDate.Value.Year;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool CanSave()
        {
            return
                !String.IsNullOrWhiteSpace(FirstName) &&
                !String.IsNullOrWhiteSpace(LastName) &&
                !String.IsNullOrWhiteSpace(PhoneNumber) &&
                !String.IsNullOrWhiteSpace(MobileNumber) &&
                Age > 15 && Age < 100;
        }

        private void OnClear()
        {
            FirstName = String.Empty;
            LastName = String.Empty;
            PhoneNumber = String.Empty;
            MobileNumber = String.Empty;
            BirthDate = null;
        }

        private void OnSave()
        {
            // Save the contact to database here...

            var contactName = FirstName + " " + LastName;
            var dialog = new SuccessView();
            dialog.DataContext = new SuccessViewModel(contactName);
            dialog.ShowDialog();

            OnClear();
        }
    }
}