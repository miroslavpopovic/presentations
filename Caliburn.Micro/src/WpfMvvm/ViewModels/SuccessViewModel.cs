using System.Windows;
using System.Windows.Input;
using WpfMvvm.Infrastructure;

namespace WpfMvvm.ViewModels
{
    public class SuccessViewModel
    {
        public SuccessViewModel(string contactName)
        {
            ContactName = contactName.ToUpper();
            CloseCommand = new DelegateCommand(OnClose);
        }

        public ICommand CloseCommand { get; set; }

        public string ContactName { get; set; }

        private static void OnClose()
        {
            MessageBox.Show(
                "How to close view from here???", "Closing view", MessageBoxButton.OK, MessageBoxImage.Question);
        }
    }
}